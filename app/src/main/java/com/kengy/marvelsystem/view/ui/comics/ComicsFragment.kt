package com.kengy.marvelsystem.view.ui.comics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kengy.marvelsystem.Model.Images
import com.kengy.marvelsystem.Model.Results
import com.kengy.marvelsystem.R
import com.kengy.marvelsystem.controller.Comics_Cont
import com.kengy.marvelsystem.controller.DialogCharacter
import com.kengy.marvelsystem.utils.GenericImagesDialog
import com.kengy.marvelsystem.utils.Preferencias
import com.kengy.marvelsystem.view.AdapterComics
import kotlinx.android.synthetic.main.fragment_comics.*
import kotlinx.android.synthetic.main.fragment_comics.view.*


class ComicsFragment : Fragment() {

    private lateinit var comicsViewModel: ComicsViewModel
    var oComicsCont: Comics_Cont = Comics_Cont()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        comicsViewModel =
            ViewModelProviders.of(this).get(ComicsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_comics, container, false)

        val prefCharacter = Preferencias(this.context)
        val idChar = prefCharacter.getCharId()
        root.pb_act_comics.visibility = ProgressBar.VISIBLE
        observador(idChar, root)
        return root

    }

    private fun observador(characterId: Int, root: View) {

        oComicsCont.getComics(characterId)
        oComicsCont.listComics.observe(this, Observer {

            populaAdapterComics(it.data.results)
            root.pb_act_comics.visibility = ProgressBar.GONE

        })

    }

    private fun testeOnclick(results: Results) {
        Toast.makeText(
            parentFragment?.context,
            "That is my Comic's ID:  ${results.id}",
            Toast.LENGTH_LONG
        ).show()

        val dlg = GenericImagesDialog(results.images)
        dlg.show(activity!!.supportFragmentManager, "Teste")


    }

    private fun populaAdapterComics(results: List<Results>) {

        recy_list_comics.adapter =
            AdapterComics(results, { resuItem: Results -> testeOnclick(resuItem) })
        recy_list_comics.layoutManager = LinearLayoutManager(this.context)

    }

    private fun normalizaLinkImage(lstImage: List<Images>): List<String> {

        val lstStrImg = mutableListOf("")
        var i =0
        val dimensionImg = "portrait_uncanny"
        lstImage.forEach {
            lstStrImg[i] = "${it.path}/$dimensionImg.${it.extension}"
            i++
        }
        return lstStrImg
    }


}