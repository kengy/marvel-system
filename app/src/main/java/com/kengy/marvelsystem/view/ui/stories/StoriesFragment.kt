package com.kengy.marvelsystem.view.ui.stories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kengy.marvelsystem.Model.Results
import com.kengy.marvelsystem.R
import com.kengy.marvelsystem.controller.Stories_Cont
import com.kengy.marvelsystem.utils.Preferencias
import com.kengy.marvelsystem.view.AdapterComics
import com.kengy.marvelsystem.view.AdapterStories
import kotlinx.android.synthetic.main.fragment_comics.*
import kotlinx.android.synthetic.main.fragment_comics.view.*
import kotlinx.android.synthetic.main.fragment_stories.*
import kotlinx.android.synthetic.main.fragment_stories.view.*

class StoriesFragment : Fragment() {

    private lateinit var dashboardViewModel: StoriesViewModel

    val oStoriesCont = Stories_Cont()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(StoriesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_stories, container, false)
        val prefCharacter = Preferencias(this.context)
        val idChar = prefCharacter.getCharId()
        root.pb_act_stories.visibility = ProgressBar.VISIBLE
        observador(idChar, root)
        return root
    }

    fun observador(characterId: Int, root: View) {

        oStoriesCont.getStories(characterId)
        oStoriesCont.listStories.observe(this, Observer {

            populaAdapterStories(it.data.results)
            root.pb_act_stories.visibility = ProgressBar.GONE
        })
    }

    fun testeOnclick(results: Results) {
        Toast.makeText(
            parentFragment?.context,
            "That is my Comic's ID:  ${results.id}",
            Toast.LENGTH_LONG
        ).show()
    }

    fun populaAdapterStories(results: List<Results>) {

        recy_list_stories.adapter =
            AdapterStories(results, { resuItem: Results -> testeOnclick(resuItem) })
        recy_list_stories.layoutManager = LinearLayoutManager(this.context)

    }
}