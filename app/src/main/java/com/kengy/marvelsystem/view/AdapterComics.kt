package com.kengy.marvelsystem.view

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kengy.marvelsystem.Model.Results
import com.kengy.marvelsystem.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_comics.view.*

class AdapterComics(private val results: List<Results>, val clickListener: (Results) -> Unit) :
    RecyclerView.Adapter<AdapterComics.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_comics, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {

        return results.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(results[position], clickListener)

    }

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

        fun bindData(results: Results, clickListener: (Results) -> Unit) {

            itemView.txt_adpt_title_comic.text = results.title
            itemView.txt_adpt_page_count_comic.text = results.pageCount.toString()
            itemView.setOnClickListener { clickListener(results) }

            if (results.images?.size != 0 && results.images != null) {
                val dimensionPicXlarge = "portrait_xlarge"
                val extensionPic = results.images?.get(0)?.extension
                val pathUrl = results.images?.get(0)?.path
                val url =
                    "$pathUrl/${dimensionPicXlarge}.$extensionPic"
                Picasso.get().load(Uri.parse(url)).into(itemView.img_adpt_comics)
            }
        }
    }
}

