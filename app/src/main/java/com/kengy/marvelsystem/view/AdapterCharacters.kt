package com.kengy.marvelsystem.view


import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kengy.marvelsystem.Model.Results
import com.kengy.marvelsystem.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_characters.view.*
import kotlinx.android.synthetic.main.adapter_comics.view.*

class AdapterCharacters(private val results: List<Results>, val onClick: (Results) -> Unit) :
	RecyclerView.Adapter<AdapterCharacters.ViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		var view =
			LayoutInflater.from(parent.context).inflate(R.layout.adapter_characters, parent, false)
		return ViewHolder(view)
	}

	override fun getItemCount(): Int {

		return results.size
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bindData(results[position], onClick)
	}


	class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

		var url: String =
			"http://i.annihil.us/u/prod/marvel/i/mg/d/d0/5269657a74350/portrait_xlarge.jpg"


		fun bindData(results: Results, onClick: (Results) -> Unit) {


			itemView.txt_adpt_name_character.text = results.name
			itemView.txt_adpt_id_character.text = results.id.toString()
			itemView.setOnClickListener { onClick(results) }

			val dimensionPicXlarge = "portrait_xlarge"
			val extensionPic = results.thumbnail.extension
			val pathUrl = results.thumbnail.path
			val url =
				"$pathUrl/${dimensionPicXlarge}.$extensionPic"

			Picasso.get().load(Uri.parse(url)).into(itemView.img_adpt_character)


		}


	}

}

