package com.kengy.marvelsystem.utils

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.kengy.marvelsystem.Model.Images
import com.kengy.marvelsystem.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.frag_dlg_character.view.*

class GenericImagesDialog() : DialogFragment() {

    var lstImages: List<Images>? = null

    constructor(lstImages: List<Images>?) : this() {

        this.lstImages = lstImages
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = activity?.layoutInflater!!.inflate(R.layout.frag_dlg_character, null)

        val dialog = AlertDialog.Builder(activity)
        dialog.setTitle("Title Dialog sucker").setPositiveButton("OK Zé", { teste, teste1 ->

        }).setCancelable(false)
            .setView(view)

        Picasso.get().load(Uri.parse(normalizaUlrImagens(lstImages)[0])).into(view.img_frag_dlg_char)


        return dialog.create()
    }

    private fun normalizaUlrImagens(listaIni: List<Images>?): List<String> {

        val listUrl: MutableList<String> = mutableListOf()
        val dimensionPicXlarge = "portrait_xlarge"
        val dimensionPicXUncanny = "portrait_uncanny"

        listaIni?.forEach {
            listUrl.add("${it.path}/${dimensionPicXUncanny}.${it.extension}")
        }
        return listUrl
    }
}