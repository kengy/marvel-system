package com.kengy.marvelsystem.utils

import java.math.BigInteger
import java.security.MessageDigest

class Utils {

	val apiPulbicKey = "133c10bc8be1dbd4a565eee86a528b06"
	val apiPrivateKey = "d8ee0cbca93b91db7657f7855ef37a6c1440912f"


	fun getHashMD5(ts: String) = "$ts$apiPrivateKey$apiPulbicKey".md5()


	fun String.md5(): String {
		val md = MessageDigest.getInstance("MD5")
		return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
	}
}