package com.kengy.marvelsystem.utils

import android.content.Context


class Preferencias(context: Context?) {

    companion object {
        val PREF_CHARACTER = "com.kengy.marvelsystem.prefs"
        val pref_id_character = "pref_id_character"

    }

    val preference = context?.getSharedPreferences(PREF_CHARACTER, Context.MODE_PRIVATE)

    fun getCharId(): Int {
        return preference!!.getInt(pref_id_character, 0)
    }

    fun setCharId(charId: Int) {
        val editor = preference?.edit()
        editor?.putInt(pref_id_character, charId)
        editor?.apply()

    }
}