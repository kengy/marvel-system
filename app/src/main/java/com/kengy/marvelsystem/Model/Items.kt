package com.kengy.marvelsystem.Model

class Items(
	var resourceURI: String,
	var name: String
) {

	var role: String = ""
	var type: String = ""

	// Constructor for COMICS CHARACTER
	constructor(name: String, resourceURI: String, role: String) : this(name, resourceURI) {
		this.role = role

	}
}

