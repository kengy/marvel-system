package com.kengy.marvelsystem.Model

class Data(
	var offset: Int,
	var limit: Int,
	var total: Int,
	var count: Int,
	var results: List<Results>
)