package com.kengy.marvelsystem.Model

class myCharacter(
    var code: Int,
    var status: String,
    var copyright: String,
    var attributionText: String,
    var attributionHTML: String,
    var etag: String,
    var data: Data

)