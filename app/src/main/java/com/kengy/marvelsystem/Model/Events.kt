package com.kengy.marvelsystem.Model

class Events(
	var available: String,
	var collectionURI: String,
	var items: List<Items>,
	var returned: Int
)