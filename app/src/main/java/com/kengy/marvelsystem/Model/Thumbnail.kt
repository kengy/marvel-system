package com.kengy.marvelsystem.Model

class Thumbnail(
	var path: String,
	var extension: String
)