package com.kengy.marvelsystem.Model

import java.util.*

class Results(
    var id: Int,
    var description: String,
    var modified: String,
    var thumbnail: Thumbnail,
    var resourceURI: String,
    var series: Series,
    var stories: Stories,
    var events: Events,
    var urls: List<Urls>
) {

    var name: String = ""
    var comics: Comics? = null


    // Constructor CHARACTER
    constructor(
        id: Int,
        description: String,
        modified: String,
        thumbnail: Thumbnail,
        resourceURI: String,
        series: Series,
        stories: Stories,
        events: Events,
        urls: List<Urls>,
        name: String,
        comics: Comics
    ) : this(
        id,
        description,
        modified,
        thumbnail,
        resourceURI,
        series,
        stories,
        events,
        urls

    ) {
        this.name = name
        this.comics = comics
    }

    var digitalId: Int = 0
    var title: String = ""
    var issueNumber: Int = 0
    var variantDescription: String = ""
    var isbn: String = ""
    var upc: String = ""
    var diamondCode: String = ""
    var ean: String = ""
    var issn: String = ""
    var format: String = ""
    var pageCount: Int = 0
    var textObjects: List<TextObjects>? = null
    var variants: List<Variants>? = null
    var dates: List<Dates>? = null
    var prices: List<Prices>? = null
    var images: List<Images>? = null
    var creators: Creators? = null
    var characters: Characters? = null
    var type: String = ""

    // Constructor COMICS
    constructor(
        id: Int,
        description: String,
        modified: String,
        thumbnail: Thumbnail,
        resourceURI: String,
        series: Series,
        stories: Stories,
        events: Events,
        urls: List<Urls>,
        digitalId: Int,
        title: String,
        issueNumber: Int,
        variantDescription: String,
        isbn: String,
        upc: String,
        diamondCode: String,
        ean: String,
        issn: String,
        format: String,
        pageCount: Int,
        textObjects: List<TextObjects>,
        variants: List<Variants>,
        dates: List<Dates>,
        prices: List<Prices>,
        images: List<Images>,
        creators: Creators,
        characters: Characters,
        type: String


    ) : this(
        id,
        description,
        modified,
        thumbnail,
        resourceURI,
        series,
        stories,
        events,
        urls
    ) {

        this.digitalId = digitalId
        this.title = title
        this.issueNumber = issueNumber
        this.issueNumber = issueNumber
        this.variantDescription = variantDescription
        this.isbn = isbn
        this.upc = upc
        this.diamondCode = diamondCode
        this.ean = ean
        this.issn = issn
        this.format = format
        this.pageCount = pageCount
        this.textObjects = textObjects
        this.variants = variants
        this.dates = dates
        this.prices = prices
        this.images = images
        this.creators = creators
        this.characters = characters
        this.type = type

    }
}