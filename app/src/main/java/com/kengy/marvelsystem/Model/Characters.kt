package com.kengy.marvelsystem.Model

class Characters(
	var available: Int,
	var collectionURI: String,
	var items: List<Items>,
	var returned: Int
)