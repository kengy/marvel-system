package com.kengy.marvelsystem.retrofit

import com.kengy.marvelsystem.Model.myStoriesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StoriesServices {
    @GET("/v1/public/characters/{characterId}/stories")
    fun getStories(
        @Path("characterId") characterId: Int,
        @Query("limit") limit: Int,
        @Query("ts") ts: String,
        @Query("apikey") apikey: String,
        @Query("hash") hash: String
    ): Call<myStoriesResponse>
}