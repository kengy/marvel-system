package com.kengy.marvelsystem.retrofit


import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetWorkUtils {

    companion object {

        fun generateUrl(url: String): Retrofit {

            return Retrofit.Builder().baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create()).build()
        }
    }
}