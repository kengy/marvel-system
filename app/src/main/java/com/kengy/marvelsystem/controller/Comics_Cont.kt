package com.kengy.marvelsystem.controller

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kengy.marvelsystem.Model.myComicsResponse
import com.kengy.marvelsystem.retrofit.ComicsServices
import com.kengy.marvelsystem.retrofit.NetWorkUtils
import com.kengy.marvelsystem.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Comics_Cont : ViewModel() {

    val apiPulbicKey = "133c10bc8be1dbd4a565eee86a528b06"
    val apiPrivateKey = "d8ee0cbca93b91db7657f7855ef37a6c1440912f"
    var utils = Utils()

    var listComics = MutableLiveData<myComicsResponse>()
//contexto: Context, callBack : PersonagensInterface

    fun getComics(idCharacter: Int) {

        val ts = System.currentTimeMillis().toString()
        val url = "https://gateway.marvel.com"
        val retrofit = NetWorkUtils.generateUrl(url)
        val service = retrofit.create(ComicsServices::class.java)
        val call = service.getComics(idCharacter, 20, ts, apiPulbicKey, utils.getHashMD5(ts))

        call.enqueue(object : Callback<myComicsResponse> {

            override fun onFailure(call: Call<myComicsResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<myComicsResponse>,
                response: Response<myComicsResponse>
            ) {

                if (response.body() != null) {
                    val myComics = response.body()

                    if (myComics != null) {
                        listComics.value = myComics

                    }
                }
            }

        })

    }
}