package com.kengy.marvelsystem.controller

import androidx.lifecycle.MutableLiveData
import com.kengy.marvelsystem.Model.myStoriesResponse
import com.kengy.marvelsystem.retrofit.NetWorkUtils
import com.kengy.marvelsystem.retrofit.StoriesServices
import com.kengy.marvelsystem.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Stories_Cont {

    val apiPulbicKey = "133c10bc8be1dbd4a565eee86a528b06"
    var utils = Utils()

    var listStories = MutableLiveData<myStoriesResponse>()

    fun getStories(idCharacter: Int) {

        val ts = System.currentTimeMillis().toString()
        val url = "https://gateway.marvel.com"
        val retrofit = NetWorkUtils.generateUrl(url)
        val service = retrofit.create(StoriesServices::class.java)
        val call = service.getStories(idCharacter, 20, ts, apiPulbicKey, utils.getHashMD5(ts))

        call.enqueue(object : Callback<myStoriesResponse> {

            override fun onFailure(call: Call<myStoriesResponse>, t: Throwable) {
                var teste = " "
                teste = "44444"

            }

            override fun onResponse(
                call: Call<myStoriesResponse>,
                response: Response<myStoriesResponse>
            ) {

                if (response.body() != null) {
                    val myStories = response.body()

                    if (myStories != null) {
                        listStories.value = myStories

                    }
                }
            }

        })

    }
}