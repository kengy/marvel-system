package com.kengy.marvelsystem.controller

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kengy.marvelsystem.Model.Images
import com.kengy.marvelsystem.Model.myCharacter
import com.kengy.marvelsystem.R
import com.kengy.marvelsystem.interfaces.PersonagensInterface
import com.kengy.marvelsystem.retrofit.CharacterServices
import com.kengy.marvelsystem.retrofit.NetWorkUtils
import com.kengy.marvelsystem.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


//https://gateway.marvel.com/v1/public/characters?name=hulk&ts=1571505395921&apikey=133c10bc8be1dbd4a565eee86a528b06&hash=eb6f4d83322fcf7d9ef7541dfe9aa604


class Personagens_Cont : ViewModel() {

    val apiPulbicKey = "133c10bc8be1dbd4a565eee86a528b06"
    val apiPrivateKey = "d8ee0cbca93b91db7657f7855ef37a6c1440912f"
    var utils = Utils()
    var listaItemCharacters = MutableLiveData<myCharacter>()
//    val callBack: PersonagensInterface? = null

    fun getDataCharacterByName(name: String, context: Context) {

        val ts = System.currentTimeMillis().toString()
        val url = "https://gateway.marvel.com"
        val retrofit = NetWorkUtils.generateUrl(url)
        val service = retrofit.create(CharacterServices::class.java)
        val call = service.getCharacter(name, 100, ts, apiPulbicKey, utils.getHashMD5(ts))

        call.enqueue(object : Callback<myCharacter> {
            override fun onFailure(call: Call<myCharacter>, t: Throwable) {

                Toast.makeText(context, "Deu ruim ao trazer o ${name}", Toast.LENGTH_LONG)
                    .show()
            }

            override fun onResponse(call: Call<myCharacter>, response: Response<myCharacter>) {

                if (response.body() != null) {
                    val myCharacter = response.body()

                    if (myCharacter != null) {
                        listaItemCharacters.value = myCharacter

                    }
//                    callBack?.returnRequisicao(response.body() as myCharacter)

                }
            }

        })
    }
}

class DialogCharacter  : DialogFragment() {



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = activity?.layoutInflater!!.inflate(R.layout.frag_dlg_character, null)
        val dialog = AlertDialog.Builder(activity)
        dialog.setTitle("Title Dialog sucker").setPositiveButton("Passo", { teste, teste1 ->

        }).setCancelable(false)
            .setView(view)
        return dialog.create()

    }
}