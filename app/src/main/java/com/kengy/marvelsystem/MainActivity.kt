package com.kengy.marvelsystem

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kengy.marvelsystem.Model.Images
import com.kengy.marvelsystem.Model.Results
import com.kengy.marvelsystem.Model.myCharacter
import com.kengy.marvelsystem.controller.DialogCharacter
import com.kengy.marvelsystem.controller.Personagens_Cont
import com.kengy.marvelsystem.interfaces.PersonagensInterface
import com.kengy.marvelsystem.utils.Preferencias
import com.kengy.marvelsystem.view.AdapterCharacters
import com.kengy.marvelsystem.view.BottomNavPrincpal
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val apiPulbicKey = "133c10bc8be1dbd4a565eee86a528b06"
    val apiPrivateKey = "d8ee0cbca93b91db7657f7855ef37a6c1440912f"


    var prefCharacter: Preferencias? = null
    //var prefCharacter = this.getSharedPreferences(Preferencias.PREF_CHARACTER,0)
    var oPersonCont = Personagens_Cont()
    lateinit var oDialogCharacter: DialogCharacter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        observador()
        btn_testeAPI.setOnClickListener(this)
        prefCharacter = Preferencias(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.btn_testeAPI -> {
                pb_act_charachter.visibility = ProgressBar.VISIBLE
                getCharByName(et_char_name.text.toString())

            }
        }
    }

    private fun getCharByName(name: String) {
        oPersonCont.getDataCharacterByName(name, this)

    }

    private fun observador() {


        oPersonCont.listaItemCharacters.observe(this, Observer {

            if (it.data.results.isNotEmpty()) {
                loadRecycleCharacters(this, it.data.results)
            } else {

                callMyDialogFragment()
                recy_list_item_characters.isGone
                Toast.makeText(
                    this,
                    "${et_char_name.text} não pode ser encontrado",
                    Toast.LENGTH_LONG
                ).show()
                clearRcyChar()
            }
            pb_act_charachter.visibility = ProgressBar.GONE
        })
    }

    private fun clearRcyChar() {
        recy_list_item_characters.adapter = null
    }

//    private fun carragardados(name : String) {
//        Personagens_Cont().getDataCharacterByName(name ,  object : PersonagensInterface {
//            override fun returnRequisicao(myCharacter: myCharacter) {
//                loadRecycleCharacters(this@MainActivity, myCharacter.data.results)
//            }
//        })
//    }


    private fun callComicsScreen(result: Results) {
        val it = Intent(this, BottomNavPrincpal::class.java)
        prefCharacter?.setCharId(result.id)
        startActivity(it)
    }

    private fun loadRecycleCharacters(context: Context, result: List<Results>) {

        recy_list_item_characters.adapter =
            AdapterCharacters(result, { resultItem: Results -> callComicsScreen(resultItem) })
        recy_list_item_characters.layoutManager = LinearLayoutManager(this)
    }

    fun callMyDialogFragment() {

        val dlg = DialogCharacter()
        dlg.show(supportFragmentManager, "Teste")
    }


}


